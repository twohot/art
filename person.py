"""Class Objects for SRA People"""

# name = first, middle, sur, nick
# contact = [addr, email, phone]
# photo  = str
# link = [spouse, sponsor, guardian, parent]
# event = [birthday]
# religion = str

from base import Properties
from contact import Contact
from event import Event


class Bio:
    """Biological Data"""

    def __init__(self):
        """Initialize Bio"""

        self.__bio = {
            "record": None,
            "sex": None,
            "maritalstatus": None,
            "genotype": None,
            "bloodgroup": None,
        }

    def update(self, datatuple=False, **kwargs):
        """Set the Medical Biodata"""

        if datatuple:
            if isinstance(datatuple, tuple) and len(datatuple) == 5:
                (
                    self.__bio["record"],
                    self.__bio["sex"],
                    self.__bio["maritalstatus"],
                    self.__bio["genotype"],
                    self.__bio["bloodgroup"],
                ) = datatuple
                return True
            return False
        if kwargs:
            for key, value in kwargs.items():
                if value and key in self.__bio:
                    if not isinstance(value, int):
                        msg = "Expected an Integer ID for {0}. Got {1}"
                        raise TypeError(msg.format(key, type(value)))
                    self.__bio[key] = value
            return True
        return False

    @property
    def bloodgroup(self):
        """Returns Str representing the bloodgroup of Bio Object"""

        return self.__bio["bloodgroup"]

    @property
    def genotype(self):
        """Returns Str representing the genotype of Bio Object"""

        return self.__bio["genotype"]

    @property
    def maritalstatus(self):
        """Returns Str representing the sex of Bio Object"""

        return self.__bio["maritalstatus"]

    @property
    def sex(self):
        """Returns Str representing the sex of Bio Object"""

        return self.__bio["sex"]

    def data(self):
        """Returns a tuple of Medical Biodata"""

        return (
            self.__bio["record"],
            self.__bio["sex"],
            self.__bio["maritalstatus"],
            self.__bio["genotype"],
            self.__bio["bloodgroup"],
        )


class Names:
    """A Person's Name"""

    def __init__(self):
        """Initialize Names"""

        self.__names = {
            "first": None,
            "middle": None,
            "nick": None,
            "sur": None,
            "prefix": None,
            "suffix": None,
        }

    def __call__(self):
        """Return formatted name when called"""

        pattern = "{0}, {1} {2}"
        last, first, mid = self.full()[1:4]
        return pattern.format(last.upper(), first.title(), mid.title())

    def update(self, datatuple=False, **kwargs):
        """Set all name variables from data tuple"""

        if datatuple:
            if isinstance(datatuple, tuple) and len(datatuple) == 6:
                (
                    self.__names["prefix"],
                    self.__names["sur"],
                    self.__names["first"],
                    self.__names["middle"],
                    self.__names["suffix"],
                    self.__names["nick"],
                ) = datatuple
                return True
            return False
        if kwargs:
            for key, value in kwargs.items():
                if value and key in self.__names:
                    if key in ["record", "prefix", "suffix"] and not isinstance(
                        value, int
                    ):
                        msg = "Expected Integer for {0}, Got {1}."
                        raise TypeError(msg.format(key, type(value)))
                    self.__names[key] = value
            return True
        return False

    @property
    def first(self):
        """Return first name"""

        return self.__names["first"]

    @property
    def middle(self):
        """Return middle name"""

        return self.__names["middle"]

    @property
    def sur(self):
        """Return surname"""

        return self.__names["sur"]

    @property
    def nick(self):
        """Return nickname"""

        return self.__names["nickname"]

    def full(self):
        """Return a tuple of this Object's names"""

        return (
            self.__names["prefix"],
            self.__names["sur"],
            self.__names["first"],
            self.__names["middle"],
            self.__names["suffix"],
            self.__names["nick"],
        )


class Person:
    """A Human"""

    def __init__(self):
        """Initialize Person"""

        self.__name = Names()
        self.__bio = Bio()
        self.__contact = Properties(Contact, "Contacts")
        self.__photo = None  # Id
        self.__link = Properties(Person, "Connections")
        self.__event = Properties(Event, "Events")
        self.__religion = None  # Id

    @property
    def name(self):
        """Return a Person's name in a specified format"""

        return self.__name

    @property
    def bio(self):
        """Return a Person's biodata"""

        return self.__bio

    @property
    def contact(self):
        """Return the Object Mgr for Person's contacts"""

        return self.__contact

    @property
    def photo(self):
        """Return a Person's image"""

        return self.__photo

    @photo.setter
    def photo(self, value):
        """Set the Photo Id for this Student"""

        if isinstance(value, int):
            self.__photo = value

    @property
    def link(self):
        """Return the Object that manages Person's Connections"""

        return self.__link

    @property
    def event(self):
        """Return the Object that manages Person's Events"""

        return self.__event

    @property
    def religion(self):
        """Return Str representation of Person's religion"""

        return self.__religion

    @religion.setter
    def religion(self, value):
        """Return Str representation of Person's religion"""

        if isinstance(value, int):
            self.__religion = value
