"""Classes and Methods for Managing Records"""


import sqlite3
import psycopg2
from config import CONFIG


class Records:
    """Database Broker"""

    def __init__(self, user):
        """initialization"""

        self.__user = None
        self.__connection = None
        self.__config = CONFIG

        if isinstance(user, str):
            self.__user = user

    def __enter__(self):
        """Enter Database Context Management"""

        login = self.__config.login()
        if False in login:
            # default to Sqlite
            self.__connection = sqlite3.connect(self.__config("path"))
        else:
            database, role, secret = login
            self.__connection = psycopg2.connect(
                dbname=database, user=role, password=secret, host="127.0.0.1"
            )
        return self

    def __exit__(self, error_type, error_value, error_traceback):
        """Exiting Context Manager"""

        if [error_type, error_value, error_traceback].count(None) < 3:
            # Something really went wrong
            err_msg = "Type: {0}, Value: {1}, Traceback: {2}"
            print(err_msg.format(error_type, error_value, error_traceback))
        self.__connection.close()

    def cursor(self):
        """Get a connection cursor"""

        return self.__connection.cursor()

    def enrolled(self):
        """Return tuple of courses enrolled by student"""

        cursor = self.cursor()
        cursor.execute(
            """
            SELECT RegisterID
                 , Session
                 , YearTypeFK
                 , CourseID
                 , Code
                 , ReferTo
            FROM CourseRegister
                JOIN Course
                    ON Course.CourseID = CourseRegister.CourseFK
            WHERE StudentFK = %s
            ORDER BY Session
                   , CourseID;
            """,
            [self.__user],
        )
        output = cursor.fetchall()
        cursor.close()
        return output

    def programme(self):
        """Return tuple of programme courses"""

        cursor = self.cursor()
        cursor.execute(
            """
            SELECT CourseFK
                 , Code
                 , Title
                 , Description
                 , PeriodFK
                 , Unit
                 , Stage
                 , Elective
                 , UTME
                 , DirectEntry
            FROM
            (
                SELECT CourseFK
                     , Unit
                     , Stage
                     , Elective
                     , UTME
                     , DirectEntry
                FROM Programme
                WHERE VersionFK =
                (
                    SELECT Base
                    FROM Curricula
                    WHERE VersionID =
                    (
                        SELECT Programme FROM Student WHERE StudentID = %s
                    )
                )
                UNION
                SELECT CourseFK
                     , Unit
                     , Stage
                     , Elective
                     , UTME
                     , DirectEntry
                FROM Programme
                WHERE VersionFK =
                (
                    SELECT Programme FROM Student WHERE StudentID = %s
                )
                EXCEPT
                SELECT CourseFK
                     , Unit
                     , Stage
                     , Elective
                     , UTME
                     , DirectEntry
                FROM Programme
                WHERE VersionFK =
                (
                    SELECT Programme FROM Student WHERE StudentID = %s
                )
                      AND Diff = -1
            ) AS Curriculum
                JOIN Course
                    ON Course.CourseID = Curriculum.CourseFK
            ORDER BY Stage
                   , CourseFK;
            """,
            [self.__user] * 3,
        )
        output = cursor.fetchall()
        cursor.close()
        return output

    def programmerules(self):
        """Return tuple of programme courses"""

        cursor = self.cursor()
        cursor.execute(
            """
            SELECT DegreeFK
                 , SubjectFK
                 , MaximumLoad
                 , MaximumYear
                 , PeriodFK
                 , Stage
                 , MaxLoad
                 , MaxElective
            FROM Curricula
                JOIN PeriodRule
                    ON Curricula.VersionID = PeriodRule.ProgrammeFK
            WHERE VersionID =
            (
                SELECT Programme FROM Student WHERE StudentID = %s
            );
            """,
            [self.__user],
        )
        output = cursor.fetchall()
        cursor.close()
        return output

    def results(self):
        """Return tuple of all Results pertaining to self.__user"""

        cursor = self.cursor()
        cursor.execute(
            """
            SELECT TypeFK
                 , Session
                 , Code
                 , ClassworkScore
                 , ExamScore
            FROM
            (
                SELECT TypeFK
                     , ResultRelease.Session  as Session
                     , ResultRelease.CourseFK as Course
                     , ClassworkScore
                     , ExamScore
                FROM ResultRelease
                    JOIN ResultSubmit
                        ON ResultRelease.SubmitFK = SubmitID
                WHERE StudentFK = %s
                UNION
                SELECT TypeFK
                     , ResultHold.Session
                     , ResultHold.CourseFK
                     , ClassworkScore
                     , ExamScore
                FROM ResultHold
                    JOIN ResultSubmit
                        ON ResultHold.SubmitFK = SubmitID
                WHERE StudentFK = %s
            ) AS Res
                JOIN Course
                    ON Res.Course = Course.CourseID
            ORDER BY Session
                   , CourseID;
            """,
            [self.__user] * 2,
        )
        output = cursor.fetchall()
        cursor.close()
        return output

    def graderule(self):
        """Return Tuple of Grading rule applicable to the Entry Set"""

        cursor = self.cursor()
        cursor.execute(
            """
            SELECT LowerBounds
                 , UpperBounds
                 , Grade
                 , GradePoint
            FROM GradeRule
            WHERE LaunchSession <=
            (
                SELECT EntrySet FROM Student WHERE StudentID = %s
            )
                  AND ClosureSession >=
                  (
                      SELECT EntrySet FROM Student WHERE StudentID = %s
                  )
                  OR ClosureSession IS NULL;
            """,
            [self.__user] * 2,
        )
        output = cursor.fetchall()
        cursor.close()
        return output

    def student(self):
        """Return tuple of Student's Core data"""

        cursor = self.cursor()
        cursor.execute(
            """
            SELECT *
            FROM Student
            WHERE StudentID = %s
            """,
            [self.__user],
        )
        output = cursor.fetchall()
        cursor.close()
        return output

    def studentbio(self):
        """Return tuple of Student's Biodata"""

        cursor = self.cursor()
        cursor.execute(
            """
            SELECT BioID
                 , NamePrefixFK
                 , Surname
                 , FirstName
                 , MiddleName
                 , NameSuffixFK
                 , Nickname
                 , SexFK
                 , MaritalStatusFK
                 , GenotypeFK
                 , BloodGroupFK
                 , ReligionFK
            FROM StudentBio
            WHERE StudentFK = %s;
            """,
            [self.__user],
        )
        output = cursor.fetchall()
        cursor.close()
        return output
