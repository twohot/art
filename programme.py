"""Classes for handling Courses and their results"""


# Years will have 'paid' property to track payment per level
from base import Properties
from records import Records
from config import CONFIG

SUBJECTS, PERIODS, LEVELS, TUITION = CONFIG(
    "subjects", "periods", "levels", "tuition"
)
# LEVELTYPES = "NORMAL|REPEAT|EXTRA|MOP"
ENTRYMODE = {1: None, 2: "utme", 3: "direct", 4: "prescience", 5: "mature"}


class Period:
    """ "An academic Period (e.g. Semester or Term)"""

    def __init__(self, **kwargs):
        """Initialize this Period with an index"""

        self.__period = {
            "tuition": False,
            "courses": Properties(Course, SUBJECTS),
            "rules": None,
            "electives": 0,
        }
        for key, value in kwargs.items():
            if key in self.__period and key != "courses":
                # Don't overwrite courses Property manager
                if key == "rules" and not isinstance(value, ProgrammeRules):
                    # Making sure rules are legit
                    continue
                self.__period[key] = value

    def enrol(self, **kwargs):
        """Add a course to this Period"""

        if not self.rules:
            print("CRITICAL: Need rules to add courses to periods")
        else:
            periodunits = self.metrics[0]
            code, stage, period, units, elective = (
                kwargs["code"],
                kwargs["stage"],
                kwargs["period"],
                kwargs["units"],
                kwargs["elective"],
            )
            maxregular = self.rules.maxload(
                period, stage
            ) - self.rules.maxelect(period, stage)
            kwargs["name"] = code
            if not self.courses.has(code):
                if elective and self.electives + units <= self.rules.maxelect(
                    period, stage
                ):
                    # OK to register an elective course
                    self.courses.bear(**kwargs)
                    print("CHECK: Enrolled into an elective course")
                    self.__period["electives"] += units
                elif units + periodunits - self.electives <= maxregular:
                    # OK to register a regular course
                    self.courses.bear(**kwargs)

    @property
    def electives(self):
        """Return the total elective load in this Period"""

        return self.__period["electives"]

    @property
    def tuition(self):
        """Return the status of tuition payment"""

        return self.__period["tuition"]

    @tuition.setter
    def tuition(self, value):
        """Set the status of tuition payment"""

        if isinstance(value, bool):
            self.__period["tuition"] = value

    @property
    def courses(self):
        """Return the Register for courses in this Period"""

        return self.__period["courses"]

    @property
    def metrics(self):
        """Return performance metrics for this period"""

        totalunits = 0.0
        totalgp = 0.0
        for course in self.courses.members():
            perf = self.courses.get(course).metrics
            totalunits += perf[0]
            totalgp += perf[4]
        if not totalunits:
            return totalunits, totalgp, 0
        cgpa = totalgp / totalunits
        return totalunits, totalgp, cgpa

    @property
    def rules(self):
        """Return the Programme rules governing this Period"""

        return self.__period["rules"]


class Session:
    """A Year/Session in an Academic Program"""

    def __init__(self, **kwargs):
        """Initialize Year's variables with an index number"""

        self.__level = {
            "yeartype": None,
            "stage": None,
            "periods": Properties(Period, PERIODS),
        }

        for key, value in kwargs.items():
            if key in self.__level:
                self.__level[key] = value

    def enrol(self, rules, **kwargs):
        """Enrol a course into this session within specified period"""

        period = kwargs.get("period", False)
        if period and isinstance(rules, ProgrammeRules):
            if not self.periods.has(period):
                self.periods.bear(**{"name": period, "rules": rules})
                self.__level["yeartype"] = kwargs.get("yeartype", None)
            self.periods.get(period).enrol(**kwargs)

    @property
    def periods(self):
        """Return the Property manager for all periods in this Session"""

        return self.__level["periods"]

    @property
    def metrics(self):
        """Calculates and returns the CGPA for this Session"""

        totalunits = 0.0
        totalgp = 0.0
        for name in self.periods.members():
            perf = self.periods.get(name).metrics
            if perf:
                totalunits += perf[0]
                totalgp += perf[1]
        if not totalunits:
            return totalunits, totalgp, 0
        cgpa = totalgp / totalunits
        return totalunits, totalgp, cgpa


class Course:
    """A Course delivered in a University or School"""

    def __init__(self, **kwargs):
        """Initialize a Course Instance with a code"""

        self.__course = {
            "record": None,  # For registered course
            "id": 0,
            "code": None,
            "title": None,
            "units": None,
            "tutor": None,  # Staff id
            "referto": None,  # Get result from another session
        }

        for key, value in kwargs.items():
            if key in self.__course:
                self.__course[key] = value

        self.__result = Result()

    @property
    def code(self):
        """Return the course code"""

        return self.__course["code"]

    @property
    def number(self):
        """Return the Id of this Course"""

        return self.__course["id"]

    @property
    def title(self):
        """Return the title of this Course"""

        return self.__course["title"]

    @property
    def units(self):
        """Return the Credit Units of this Course"""

        return self.__course["units"]

    @property
    def referto(self):
        """Return an Integer representing Session bearing result of this
        course"""

        return self.__course["referto"]

    @property
    def result(self):
        """Returns the Result holder/object for this Course"""

        return self.__result

    @property
    def scores(self):
        """Return the result object for this Course"""

        return self.__result.scores

    @property
    def metrics(self):
        """Return student's performance point calculation for this Course"""

        return self.__result()

    @property
    def about(self):
        """Print formatted information about the Course"""

        space = 3
        long = 0
        for key in ["heading"] + list(self.__course.keys()):
            if len(key) > long:
                long = len(key)
        pat = "{0}{1}| {2}"
        print(pat.format("\n\nHeading", " " * (long - 7 + space), "Data"))
        print(pat.format("-" * 7, " " * (long - 7 + space), "-" * 4))
        for key, value in self.__course.items():
            print(pat.format(key, " " * (long - len(key) + space), value))


class ProgrammeRules:
    """Rules governing registrations within a programme"""

    def __init__(self, datalist=None):
        """Initialize Rules"""

        self.__rules = {
            "degree": None,
            "subject": None,
            "defaultmax": None,
            "maxyear": None,
            "rules": {},
        }

        if datalist:
            self.__rules.update(
                {
                    "degree": datalist[0][0],
                    "subject": datalist[0][1],
                    "defaultmax": datalist[0][2],
                    "maxyear": datalist[0][3],
                }
            )
            for rule in datalist:
                if rule[5] not in self.__rules["rules"]:
                    self.__rules["rules"][rule[5]] = {}
                self.__rules["rules"][rule[5]][rule[4]] = {
                    "maxload": rule[6],
                    "maxelect": rule[7],
                }

    @property
    def degree(self):
        """Return the degree Id for this rule"""

        return self.__rules["degree"]

    @property
    def subject(self):
        """Return the subject Id for this rule"""

        return self.__rules["subject"]

    @property
    def defaultmax(self):
        """Return the default maximum units per period"""

        return self.__rules["defaultmax"]

    @property
    def maxyear(self):
        """Return the maximum allowed sessions"""

        return self.__rules["maxyear"]

    def maxload(self, period, stage):
        """Return the maximum units allowed for specified period"""

        return self.__rules["rules"][stage][period]["maxload"]

    def maxelect(self, period, stage):
        """Return the maximum elective units allowed for specified period"""

        return self.__rules["rules"][stage][period]["maxelect"]


class Programme:
    """Data of Courses to be taken by student"""

    def __init__(self):
        """Initialize the Curriculum Object"""

        self.__prog = {
            "student": None,
            "mode": 0,
            "stage": 0,
            "courses": {},
            "data": {},
            "rules": ProgrammeRules(),
            "pool": ProgrammePool(),
        }

    def __load(self):
        """Load programme courses"""

        self.reset()
        prog = []
        progrules = []
        entrymode = ENTRYMODE[self.__prog["mode"]]
        msg = "INFO: Using Programme for {0} Entry Students"
        print(msg.format(entrymode.title()))

        with Records(self.student) as record:
            prog.extend(record.programme())
            progrules.extend(record.programmerules())

        if prog[0]:
            for course in prog:
                keys = (
                    "id",
                    "code",
                    "title",
                    "descr",
                    "period",
                    "units",
                    "stage",
                    "elective",
                    "utme",
                    "direct",
                )
                kwargs = {}
                kwargs.update(
                    [
                        (
                            key,
                            course[keys.index(key)],
                        )
                        for key in keys
                    ]
                )
                code, stage = kwargs["code"], kwargs["stage"]
                if stage <= self.stage and kwargs.get(entrymode, 1) == 1:
                    # Work with Courses up to Student's Level
                    # Courses must apply to student's mode-of-entry
                    if code in self.__prog["courses"]:
                        msg = "Programme has duplicate course: {}"
                        raise ValueError(msg.format(code))

                    # List of courses
                    self.__prog["courses"][code] = stage

                    # Order of Registration
                    if stage not in self.__prog["data"]:
                        self.__prog["data"][stage] = {}
                    self.__prog["data"][stage][code] = kwargs
            self.pool.copy(self.data)

        if progrules[0]:
            self.rules.__init__(progrules)

    @property
    def pool(self):
        """Return the Pool Object"""

        return self.__prog["pool"]

    def reset(self):
        """Clear all data from this Register"""

        self.__prog["courses"].clear()
        self.__prog["data"].clear()
        self.__prog["rules"].__init__()

    def get(self, code, data=None):
        """Return specific Course data as per Programme"""

        value = False
        if self.has(code):
            stage = self.__prog["courses"][code]
            value = self.__prog["data"][stage][code]
            if data:
                return value.get(data, False)
        return value

    @property
    def ready(self):
        """Return True if Programme is loaded and ready"""

        return len(self.__prog["courses"]) > 0

    @property
    def rules(self):
        """Return the rule object for this Programme"""

        return self.__prog["rules"]

    @property
    def stage(self):
        "Return the stage limit of this Programme"

        return self.__prog["stage"]

    @property
    def student(self):
        """Return the registration number of the owner"""

        return self.__prog["student"]

    def set(self, userid, stage, mode):
        """Set the owner/context of this Programmme"""

        if isinstance(userid, str) and isinstance(stage, int):
            self.__init__()  # reset
            (
                self.__prog["student"],
                self.__prog["stage"],
                self.__prog["mode"],
            ) = (userid, stage, mode)
            # Setting student loads associated programme
            self.pool.clear()
            self.__load()

    def has(self, code):
        """Return True if code is on the Programme"""

        return code in self.__prog["courses"]

    @property
    def data(self):
        """Return a Dictionary of the programme"""

        return self.__prog["data"]


class ProgrammePool:
    """Object for running checking registrations"""

    def __init__(self):
        """Initialize Checklist"""

        self.__pool = {}
        self.__total = 0
        self.__outstanding = 0

    def moot(self, code):
        """Return Course data if code is available for registration
        Return False if course was resolved"""

        state = False
        for stage in self.__pool.items():
            if code in stage[1]:
                state = stage[1][code]
                break
        return state

    def tick(self, code):
        """Tick a course/subject as passed or completed.
        The Code will be removed from the Pool"""

        for stage in self.__pool.items():
            if code in stage[1]:
                del stage[1][code]
                self.__outstanding += -1
                break

    def copy(self, progdata):
        """Copy programme register data"""

        if isinstance(progdata, dict):
            self.__pool.update(progdata)
            for stage in self.__pool.items():
                self.__total += len(stage[1])
        self.__outstanding = self.__total

    @property
    def outstanding(self):
        """Return number of unresolved courses"""

        return self.__outstanding

    def clear(self):
        """clear the pool"""

        self.__init__()

    def data(self):
        """Return a dictionary of courses in the Pool"""

        return self.__pool

    @property
    def passed(self):
        """Return number of courses passed by the student"""

        return self.__total - self.__outstanding


class Register:
    """Manage Student, Session and Course Registrations"""

    def __init__(self, number=None, stage=None, mode=None):
        """Initialize Manager. Must set number and stage before relevant engines
        can initialize completely"""

        self.__register = {
            "number": None,
            "stage": None,
            "mode": None,
            "programme": Programme(),
            "sessions": Properties(Session, LEVELS),
            "results": ResultsBroker(),
        }
        if (
            isinstance(number, str)
            and isinstance(stage, int)
            and isinstance(mode, int)
        ):
            (
                self.__register["number"],
                self.__register["stage"],
                self.__register["mode"],
            ) = (number, stage, mode)
        self.__register["results"].programme = self.__register["programme"]
        self.__register["results"].sessions = self.__register["sessions"]
        self.refresh(programme=True)

    def __load(self):
        """Load all course registrations from record"""

        with Records(self.__register["number"]) as records:
            enrolled = records.enrolled()
            if enrolled:
                for data in enrolled:
                    kwargs = {
                        "record": data[0],
                        "session": data[1],
                        "yeartype": data[2],
                        "id": data[3],
                        "code": data[4],
                        "referto": data[5],
                    }
                    session = kwargs.get("session")
                    if session and not self.sessions.has(session):
                        self.enrol(name=session)
                    self.enrol(into=session, **kwargs)

    @property
    def sessions(self):
        """Return Properties Manager for all sessions in this Register"""

        return self.__register["sessions"]

    @property
    def programme(self):
        """Return the Registered Curriculum/Programme Object"""

        return self.__register["programme"]

    def enrol(self, **kwargs):
        """Register/enrol into a Session or Course"""

        name, session, into, code = (
            kwargs.get("name", ""),
            kwargs.get("session", 0),
            kwargs.get("into", 0),
            kwargs.get("code", ""),
        )
        if name and not into:
            # Registering a Session only
            if self.sessions.count() < self.programme.rules.maxyear:
                if not isinstance(session, int):
                    msg = "Session names should be integers. Got {0}."
                    raise TypeError(msg.format(type(session)))
                self.sessions.bear(**kwargs)
            else:
                print("WARNING: Attempt to register too many sessions")

        elif into:
            # Otherwise register a Course into an existing session
            coursedata = self.__register["programme"].pool.moot(code)
            if coursedata:
                # First make-up for missing data from Programme
                for key in set(coursedata).difference(kwargs):
                    kwargs[key] = coursedata[key]
                if kwargs["stage"] <= self.programme.stage:
                    self.sessions.get(into).enrol(
                        self.programme.rules, **kwargs
                    )
                    self.results.refresh(into, kwargs["period"], code)
                else:
                    print("ILLEGAL: Student has not reached Stage-{stage}.")
        else:
            print("ERROR: Could not determine course, or session.")

    def refresh(self, programme=False):
        """Reload register information from records"""

        number = self.__register["number"]
        if number and isinstance(number, str):
            if programme:
                self.programme.set(
                    number, self.__register["stage"], self.__register["mode"]
                )
                self.results.student = number
            self.__load()

    @property
    def results(self):
        """Return the Results Broker for this Register"""

        return self.__register["results"]


class Grader:
    """Grade rule executor"""

    def __init__(self, rulelist=None):
        """initialize grader"""

        self.__rule = []

        if isinstance(rulelist, list):
            self.__rule = rulelist

    def grade(self, score):
        """Return the Grade and GradePoint of a score
        Returns a list according the loaded rules"""

        for srange in self.__rule:
            if srange[1] >= score:
                return [
                    srange[2],
                    srange[3],
                ]
        return (
            None,
            0,
        )

    def rule(self):
        """Return a list of the grade range rules"""

        return self.__rule


class Result:
    """A Registered Course Result for a specific Student"""

    def __init__(self, **kwargs):
        """initialize Result"""

        self.__scores = {
            "course": None,
            "session": None,
            "units": 0.0,
            "incourse": 0.0,
            "exam": 0.0,
        }

        self.__grader = None

        for key, value in kwargs.items():
            if key in self.__scores:
                self.__scores[key] = value

    def __call__(self):
        """Return a tuple of scores"""

        totalscore = self.total
        grade = None
        gradepoint = 0
        if self.__grader:
            grade, gradepoint = self.__grader.grade(totalscore)
        return tuple(
            [
                self.__scores["units"],
                totalscore,
                grade,
                gradepoint,
                self.__scores["units"] * gradepoint,
            ]
        )

    @property
    def scores(self):
        """Return the Scores in this result"""

        return self.__scores["incourse"], self.__scores["exam"]

    @property
    def total(self):
        """Return total score"""

        incourse, exam = (
            self.__scores["incourse"],
            self.__scores["exam"],
        )
        if not incourse or not isinstance(
            incourse,
            (
                int,
                float,
            ),
        ):
            incourse = 0
        if not exam or not isinstance(
            exam,
            (
                int,
                float,
            ),
        ):
            exam = 0
        return incourse + exam

    def gradewith(self, grader):
        """Set the grading object for this result"""

        if not isinstance(grader, Grader):
            msg = "Expected a grader object. Got {0} instead"
            raise TypeError(msg.format(type(grader)))
        self.__grader = grader


class ResultsBroker:
    """Result Compiler and broker for each Student"""

    def __init__(self):
        """Initialize result with all registered sessions"""

        self.__results = {
            "student": None,
            "sessions": None,
            "grader": Grader(),
            "programme": None,
            "record": {},
        }

    def __load(self):
        """Load results from database"""

        user = self.__results["student"]
        if user:
            with Records(user) as records:
                # Load grading rules
                rules = records.graderule()
                if rules[0]:
                    self.__results["grader"].__init__(rules)
                results = records.results()
                if results[0]:
                    self.__sortrecord(results)
        else:
            print("No context to work with. Specify the Student")

    def __sortrecord(self, resultlist):
        """Sort fresh/raw results from database"""

        self.__results["record"].clear()
        for each in resultlist:
            resultype, session, code, incourse, exam = each
            if session not in self.__results["record"]:
                self.__results["record"][session] = {}
            if resultype not in self.__results["record"][session]:
                self.__results["record"][session][resultype] = {}
            if code in self.__results["record"][session][resultype]:
                print("Student already has a result of this type")
            else:
                self.__results["record"][session][resultype][code] = {
                    "incourse": incourse,
                    "exam": exam,
                }

    def __assignrecord(self, courseobj, session, code):
        """Assign result scores to Course from sorted raw results"""

        if not isinstance(courseobj, Course):
            msg = "Expected a Course Object. Got {0}."
            raise TypeError(msg.format(type(courseobj)))

        if courseobj.referto:
            # Use non-default result for this course
            session = courseobj.referto

        resultsdata = self.__results["record"]
        if TUITION:
            resultypes = [2, 4]  # types for paying students
        else:
            # Use any type that holds scores
            resultypes = []
            if session in resultsdata:
                resultypes = list(resultsdata[session].keys())
        for typeid in resultypes:
            if typeid in resultsdata[session]:
                if code in resultsdata[session][typeid]:
                    scores = resultsdata[session][typeid][code]
                    values = {
                        "course": code,
                        "session": session,
                        "units": courseobj.units,
                    }
                    values.update(scores)
                    courseobj.result.__init__(**values)
                    courseobj.result.gradewith(self.__results["grader"])
                    break

    def refresh(self, session, period, code):
        """Refresh Results for specified course in the specified period"""

        courseobj = (
            self.sessions.get(session).periods.get(period).courses.get(code)
        )
        self.__assignrecord(courseobj, session, code)

        # Check grade and Update Programme Pool is resolved
        grade = courseobj.result()[2]
        if grade not in ["F", "f", 0, None]:
            # passed the course. Tick it off
            self.programme.pool.tick(code)

    @property
    def metrics(self):
        """final metrics including CGPA"""

        totalunits = 0
        totalgp = 0
        for name in self.sessions.members():
            perf = self.sessions.get(name).metrics
            if perf:
                totalunits += perf[0]
                totalgp += perf[1]
        if not totalunits:
            return totalunits, totalgp, 0
        cgpa = totalgp / totalunits
        return totalunits, totalgp, cgpa

    @property
    def sessions(self):
        """Return Properties Object holding Course Registrations"""

        return self.__results["sessions"]

    @sessions.setter
    def sessions(self, sessionsobj):
        """Assign relevant Sessions Object to Results Manager"""

        if isinstance(sessionsobj, Properties):
            self.__results["sessions"] = sessionsobj

    @property
    def programme(self):
        """Return the Programme Object for Results"""

        return self.__results["programme"]

    @programme.setter
    def programme(self, progobj):
        """Assign relevant Sessions Object to Results Manager"""

        if isinstance(progobj, Programme):
            self.__results["programme"] = progobj

    @property
    def student(self):
        """Return Owner's ID"""

        return self.__results["student"]

    @student.setter
    def student(self, student_id):
        """Assign relevant Sessions Object to Results Manager"""

        if isinstance(student_id, str):
            self.__results["student"] = student_id
            self.__load()

    def status(self):
        """Output a concise status report for active student"""

        header = "\nResults status for: {0}\n{1}\n"
        mileage = "Passed: {0} ({1}%); Remaining: {2} ({3}%); CGPA: {4}"
        undone = "Outstanding >| Code: {0}, @Stage: {1}, Id: {2}, Title: {3}"
        print(header.format(self.student, "-" * (21 + len(self.student))))
        passed = self.programme.pool.passed
        remains = self.programme.pool.outstanding
        total = passed + remains
        print(
            mileage.format(
                passed,
                round(passed / total * 100),
                remains,
                round(remains / total * 100),
                round(self.metrics[2], 2),
            )
        )
        for stage, course in self.programme.pool.data().items():
            for code in course:
                print(
                    undone.format(
                        code, stage, course[code]["id"], course[code]["title"]
                    )
                )
