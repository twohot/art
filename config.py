"""Database Methods for Class Asistant Bot"""

import os


HOME = os.environ.get("HOME")

# Search the following paths for Bot 'config' file
CONFPATHS = [
    HOME + "/.config/art/",
    HOME + "/.art/",
    "/etc/art/",
]

DEFAULT_DBPATH = HOME + "/.sra.db"


def parseconf(abspath):
    """Read a configuration file and return a Dict version of same"""

    readlist = []
    conf = {}

    with open(abspath, mode="r", encoding="utf-8") as confile:
        # Grab entire file as list
        readlist.extend(list(confile))

    for setting in readlist:
        if not setting.strip() or setting.startswith("#"):
            continue
        # Remove white spaces and newline chars
        key, value = [item.strip() for item in setting.split("=")]
        if value.upper() in ["YES", "1", "TRUE", "ON"]:
            value = True
        elif value.upper() in ["NO", "0", "FALSE", "OFF"]:
            value = False
        conf[key.lower()] = value
    return conf


class Config:
    """Configuration Manager Object"""

    def __init__(self):
        """Read configuration file and initialize basic
        settings from it."""

        self.__config = {}

        for path in CONFPATHS:
            search = path + "config"
            if os.path.exists(search):
                self.__config.update(parseconf(search))
                break

        server = self.__config.get("server", None)
        if server != "postgresql":
            # Default to Sqlite if Postgresql is not set
            if "path" not in self.__config:
                # Default Database filepath for Sqlite
                self.__config["path"] = DEFAULT_DBPATH

    def __call__(self, *args):
        """Return the value for the specifed key"""

        values = []
        for arg in args:
            values.append(self.__config.get(arg, None))
        if len(values) == 1:
            return values[0]
        return tuple(values)

    def server(self):
        """Return the configured server"""

        server = self.__config["server"]
        if server.startswith("postgre"):
            return "postgresql"
        return "sqlite"

    def isset(self, key):
        """Return Bool for key presence in config"""

        return key in self.__config

    def login(self):
        """Return login tuple if running Postgresql"""

        login = (
            self.__config.get("dbname", False),
            self.__config.get("dbuser", False),
            self.__config.get("dbpass", False),
        )
        if self.server() == "postgresql":
            return login
        return (
            False,
            False,
            False,
        )


CONFIG = Config()
